//
//  TWAAlphabeticallyWord.h
//  TestWordsApp
//
//  Created by Ruslan on 11/16/14.
//  Copyright (c) 2014 Ruslan. All rights reserved.
//

#import "TWAWord.h"

@interface TWAAlphabeticallyWord : TWAWord

@property (nonatomic, strong) NSString *alphabeticallyWordValue;

- (id)initWithWord:(TWAWord *)aWord;

@end
