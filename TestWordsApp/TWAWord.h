//
//  TWAWord.h
//  TestWordsApp
//
//  Created by Ruslan on 11/9/14.
//  Copyright (c) 2014 Ruslan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface TWAWord : NSManagedObject

@property (nonatomic, retain) NSString * wordValue;
@property (nonatomic, retain) NSString * wordDescription;

@end
