//
//  NSString+Alphabetically.m
//  TestWordsApp
//
//  Created by Ruslan on 11/15/14.
//  Copyright (c) 2014 Ruslan. All rights reserved.
//

#import "NSString+Alphabetically.h"

@implementation NSString (Alphabetically)

- (NSString *)alphabeticallyString {
    NSString *alphabeticallyString = nil;
    
    NSMutableArray *charArray = [NSMutableArray arrayWithCapacity:self.length];
    for (int i=0; i < self.length; ++i) {
        NSString *charStr = [self substringWithRange:NSMakeRange(i, 1)];
        [charArray addObject:charStr];
    }
    
    [charArray sortUsingComparator:^(NSString *a, NSString *b){
        return [a compare:b];
    }];
    
    alphabeticallyString = [[charArray valueForKey:@"description"] componentsJoinedByString:@""];
    
    return alphabeticallyString;
}

@end
