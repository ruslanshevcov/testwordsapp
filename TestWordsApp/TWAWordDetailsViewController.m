//
//  TWAWordDetailsViewController.m
//  TestWordsApp
//
//  Created by Ruslan on 11/9/14.
//  Copyright (c) 2014 Ruslan. All rights reserved.
//

#import "TWAWordDetailsViewController.h"

// models
#import "TWAWord.h"

@interface TWAWordDetailsViewController ()

@property (weak, nonatomic) IBOutlet UILabel *wordValueLabel;
@property (weak, nonatomic) IBOutlet UITextView *wordDescriptionTextView;

@end

@implementation TWAWordDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.wordValueLabel.text = self.currentWord.wordValue;
    self.wordDescriptionTextView.text = self.currentWord.wordDescription;
}

@end
