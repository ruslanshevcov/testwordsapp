//
//  TWAWord+Alphabetically.m
//  TestWordsApp
//
//  Created by Ruslan on 11/15/14.
//  Copyright (c) 2014 Ruslan. All rights reserved.
//

#import "TWAWord+Alphabetically.h"

// helpers
#import "NSString+Alphabetically.h"

@implementation TWAWord (Alphabetically)

- (NSString *)alphabeticallyWordValue {
    return [self.wordValue alphabeticallyString];
}

@end
