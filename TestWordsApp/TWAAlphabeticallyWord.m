//
//  TWAAlphabeticallyWord.m
//  TestWordsApp
//
//  Created by Ruslan on 11/16/14.
//  Copyright (c) 2014 Ruslan. All rights reserved.
//

#import "TWAAlphabeticallyWord.h"

// helpers
#import "NSString+Alphabetically.h"

@implementation TWAAlphabeticallyWord

@synthesize wordValue = _wordValue;
@synthesize wordDescription = _wordDescription;
@synthesize alphabeticallyWordValue = _alphabeticallyWordValue;

- (id)initWithWord:(TWAWord *)aWord {
    self = [super init];
    if (self) {
        _wordValue = aWord.wordValue;
        _wordDescription = aWord.wordDescription;
        _alphabeticallyWordValue = [aWord.wordValue alphabeticallyString];
    }
    return self;
}

@end
