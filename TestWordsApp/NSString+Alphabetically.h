//
//  NSString+Alphabetically.h
//  TestWordsApp
//
//  Created by Ruslan on 11/15/14.
//  Copyright (c) 2014 Ruslan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Alphabetically)

- (NSString *)alphabeticallyString;

@end
