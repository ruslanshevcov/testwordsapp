//
//  TWAHomeViewController.m
//  TestWordsApp
//
//  Created by Ruslan on 11/9/14.
//  Copyright (c) 2014 Ruslan. All rights reserved.
//

#import "TWAHomeViewController.h"

// app delegate
#import "TWAAppDelegate.h"

// models
#import "TWAWord.h"
//#import "TWAWord+Alphabetically.h"
#import "TWAAlphabeticallyWord.h"

// UI
#import "TWAWordDetailsViewController.h"

// helpers
#import "NSString+Alphabetically.h"

@interface TWAHomeViewController ()

@property (weak, nonatomic) IBOutlet UITableView *wordsTableView;

@property (weak, nonatomic) IBOutlet UIView *lockInterfaceView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *lockInterfaceActivityView;

@property (nonatomic, strong) NSMutableArray *datasource;
@property (nonatomic, strong) NSMutableArray *searchContent;

@end

@implementation TWAHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self updateDatasource];
    
    if ([self.datasource count]) {
        self.lockInterfaceView.hidden = YES;
    } else {
        [self.lockInterfaceActivityView startAnimating];
        self.lockInterfaceView.hidden = NO;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateDatasource)
                                                 name:kLoadWordsFromRemoteStorageNotification
                                               object:nil];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)updateDatasource {
    [self.lockInterfaceActivityView stopAnimating];
    self.lockInterfaceView.hidden = YES;
    
    [self.datasource removeAllObjects];
//    [self.datasource addObjectsFromArray:[(TWAAppDelegate *)[UIApplication sharedApplication].delegate wordFromLocalStorage]];
    NSArray *words = [(TWAAppDelegate *)[UIApplication sharedApplication].delegate wordFromLocalStorage];
    [words enumerateObjectsUsingBlock:^(TWAWord *word, NSUInteger idx, BOOL *stop) {
        TWAAlphabeticallyWord *alphabeticallyWord = [[TWAAlphabeticallyWord alloc] initWithWord:word];
        [self.datasource addObject:alphabeticallyWord];
    }];
    
    [self.searchContent removeAllObjects];
    [self.searchContent addObjectsFromArray:self.datasource];
    
    [self.wordsTableView reloadData];
}

- (NSMutableArray *)datasource {
    if (_datasource == nil) {
        _datasource = [NSMutableArray array];
    }
    return _datasource;
}

- (NSMutableArray *)searchContent {
    if (_searchContent == nil) {
        _searchContent = [NSMutableArray array];
    }
    return _searchContent;
}

#pragma mark -
#pragma mark Storyboard

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    UITableViewCell *cell = (UITableViewCell *)sender;
    NSIndexPath *indexPathOfCell = [self.wordsTableView indexPathForCell:cell];
    TWAWord *currentWord = self.searchContent[indexPathOfCell.row];

    TWAWordDetailsViewController *wordDetailsViewController = segue.destinationViewController;
    wordDetailsViewController.currentWord = currentWord;
}

#pragma mark -
#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.searchContent count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *wordCellIdentifier = @"word.cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:wordCellIdentifier];

    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:wordCellIdentifier];
    }
    
    cell.textLabel.text = ((TWAWord *)self.searchContent[indexPath.row]).wordValue;
    
    return cell;
}

#pragma mark -
#pragma mark UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {

    if (![text length]) {
        return YES;
    }
    
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[a-zA-Z]"
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    
    NSRange rangeOfFirstMatch = [regex rangeOfFirstMatchInString:text
                                                         options:0
                                                           range:NSMakeRange(0, [text length])];
    
    return (BOOL)(rangeOfFirstMatch.location != NSNotFound);
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self.searchContent removeAllObjects];
    
    if ([searchText length] >= 3) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"alphabeticallyWordValue LIKE[c] %@", [searchText alphabeticallyString]];
        [self.searchContent  addObjectsFromArray:[NSMutableArray arrayWithArray:[self.datasource filteredArrayUsingPredicate:predicate]]];
    } else {
        [self.searchContent  addObjectsFromArray:self.datasource];
    }
    [self.wordsTableView reloadData];
}

@end
