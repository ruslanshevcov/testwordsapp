//
//  main.m
//  TestWordsApp
//
//  Created by Ruslan on 11/9/14.
//  Copyright (c) 2014 Ruslan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TWAAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TWAAppDelegate class]));
    }
}
