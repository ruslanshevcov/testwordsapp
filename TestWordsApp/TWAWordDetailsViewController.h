//
//  TWAWordDetailsViewController.h
//  TestWordsApp
//
//  Created by Ruslan on 11/9/14.
//  Copyright (c) 2014 Ruslan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TWAWord;

@interface TWAWordDetailsViewController : UIViewController

@property (nonatomic, strong) TWAWord *currentWord;

@end
