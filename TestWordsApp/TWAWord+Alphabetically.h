//
//  TWAWord+Alphabetically.h
//  TestWordsApp
//
//  Created by Ruslan on 11/15/14.
//  Copyright (c) 2014 Ruslan. All rights reserved.
//

#import "TWAWord.h"

@interface TWAWord (Alphabetically)

- (NSString *)alphabeticallyWordValue;

@end
